package models

// Result - A model for handling request response's result field
type Result struct {
	Lang string   `json:"lang"`
	Word string   `json:"word"`
	Mean []string `json:"mean"`
}
