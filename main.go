package main

import (
	"fmt"
	"log"
	"os"
	"strings"
)

func main() {

	// Define word to translate
	var word string
	// Check command line arguments for word
	if len(os.Args) > 1 {
		word = strings.Join(os.Args[1:], " ")
	} else {
		// Ask user to input the word to translate
		fmt.Print("Word: ")
		// Put the input in the word object
		_, err := fmt.Scanf("%s", &word)
		if err != nil {
			log.Fatalln("Input a valid word! ", err.Error())
		}
	}
	response := Translate(word)
	if len(response.Result.Mean) == 0 {
		log.Fatalf("Sorry, no translations were found for '%s' word :(\n", word)
	}
	response.Print()
}
