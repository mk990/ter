package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"gitlab.com/shokoohi/ter/models"
)

// Translate - Send http request and bind that to a object of our response model
func Translate(word string) *models.Response {
	// Make a http client
	client := &http.Client{
		Timeout: time.Second * 10,
	}
	// Prepare request body
	requestBody, err := json.Marshal(map[string]interface{}{
		"jsonrpc": "2.0",
		"method":  "Targoman::lookupDic",
		"id":      1,
		"params":  [2]string{"sSTargomanWUI", word},
	})
	if err != nil {
		log.Fatalln("Error in making request body:", err.Error())
	}
	// Define the address
	address := "https://targoman.ir/API/"
	// Making the request
	request, err := http.NewRequest("POST", address, bytes.NewBuffer(requestBody))
	if err != nil {
		log.Fatalln("Error in making the request:", err.Error())
	}
	// Set headers
	request.Header.Set("Connection", "keep-alive")
	request.Header.Add("Content-Type", "application/json")
	// Send the request
	res, err := client.Do(request)
	if err != nil {
		log.Fatalln("Error in sending the request:", err.Error())
	}
	defer res.Body.Close()
	// Get the body
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatalf("Request failed with status code %d and empty body!\n", res.StatusCode)
	}
	// Check if response isn't success
	if res.StatusCode != http.StatusOK {
		log.Fatalf("Request failed with status code %d and error: %s!\n", res.StatusCode, string(body))
	}
	var response models.Response
	err = json.Unmarshal(body, &response)
	if err != nil {
		log.Fatalf("Failed to convert response body to the response model with statusCode %d and body %s!\n",
			res.StatusCode, string(body))
	}
	// Return the response object
	return &response
}
