# ter

A translate command-line application written in Go that translate words from any languages to the Persian and Persian to the English. This app using simpe [Targoman](https://targoman.ir/) http requests!


## Configure

### Configure your Go:

If you didn't set your $GOPATH address It's okay.
Let's do this!
First, create a directory in your home.
Like this:

```bash
mkdir -p $HOME/go/
```


Then we must set this directory's address into $GOPATH.
All you have to do is just it:

```bash
export GOPATH=$HOME/go
```

Also you must update your $PATH address.
Like this:

```bash
export PATH=$PATH:$GOPATH/go
```

Congratulations!!! You now configure your Go language.
Let's run our app...

## Run
### Run as classic way:

Oh, I see! We both like the classic way. Oh, wait! I don't mean that!
Never mind! Running our app is a better choice!
…If You have some issues with installing dependencies, Maybe this command can help you:

```bash
go env -w GO111MODULE=auto
```

Very well! After that, we now have dependencies and a build file that you can't see here. Don't worry!
Our app has been compiled at the bin/ directory in your _$GOPATH_ directory that we already added this address to our _$PATH_ address.
The exact address of our binary file is:
_$GOPATH/bin/ter_

#### Run the compiled file:

Well, After install and build our app we have a binary file,
To execute this file We can enter name of the app in command line anywhere, Then write a word to translate after the app name. Like the:

```bash
ter Hello
```

If you didn't add add the address of the Go binary directories, You must enter full address of the app instead of its name. Just like this:
```bash
$GOPATH/bin/ter Hello
```

And there We go! Our app is now working very well!!

That's it. We are done! We now have a rest API with Go!
I would be honored if you contribute to this project.

